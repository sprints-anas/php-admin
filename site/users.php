<?php
require_once('./config.php');
require_once(BASE_PATH . '/logic/posts.php');
require_once(BASE_PATH . '/logic/auth.php');
require_once(BASE_PATH . '/logic/users.php');
require_once(BASE_PATH . '/layout/header.php');

$page = isset($_REQUEST['page']) ? $_REQUEST['page'] : 1;
$page_size = 20;
$order_field = isset($_REQUEST['order_field']) ? $_REQUEST['order_field'] : 'id';
$order_by = isset($_REQUEST['order_by']) ? $_REQUEST['order_by'] : 'asc';
$q = isset($_REQUEST['q']) ? $_REQUEST['q'] : '';
function getUrl($page, $q, $order_field, $order_by)
{
    return "users.php?page=$page&q=$q&order_field=$order_field&order_by=$order_by";
}
function getBlockStatus($active) {
    if($active == 1) return "Block";
    else { 
        return "Unblock";
    }
}
$sql = "SELECT * FROM users";
$users = getUsers($sql);
$page_count = ceil(count($users) / $page_size);
?>
<!-- Page Content -->
<!-- Banner Starts Here -->
<div class="heading-page header-text">
    <section class="page-heading">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="text-content">
                        <h4>USERS</h4>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<!-- Banner Ends Here -->
<section class="blog-posts">
    <div class="container">

        <div class="row">
            <div class="col-lg-12">
                <div class="all-blog-posts">
                    <div class="row">
                        <div class="col-md-10">
                            <div class="sidebar-item search">
                            </div>
                        </div>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>User</th>
                                    <th>Username</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = ($page - 1) * $page_size+1;
                                foreach ($users as $user) {
                                    $blockStatus = getBlockStatus($user['active']);
                                    if($blockStatus == "Block") {
                                        $res = "BlockUser";
                                    }
                                    else {
                                        $res = "unBlockUser";
                                    }
                                    echo "<tr>
                                    <td>$i</td>
                                    <td>{$user['name']}</td>
                                    <td>{$user['username']}</td>
                                    <td>
                                    <a href='./api/$res.php?id={$user['id']}' class='btn btn-secondary'>$blockStatus</a>
                                    <a onclick='return confirm(\"Are you sure ?\")' href='./api/deleteUser.php?id={$user['id']}' class='btn btn-danger'>Delete</a>
                                    </td>
                                    </tr>";

                                    $i++;
                                }

                                ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-lg-12">
                        <ul class="page-numbers">
                            <?php
                            $prevUrl = getUrl($page - 1, $q, $order_field, $order_by);
                            $nxtUrl = getUrl($page + 1, $q, $order_field, $order_by);

                            if ($page > 1) echo "<li><a href='{$prevUrl}'><i class='fa fa-angle-double-left'></i></a></li>";

                            for ($i = 1; $i <= $page_count; $i++) {
                                $url = getUrl($i, $q, $order_field, $order_by);
                                echo "<li class=" . ($i == $page ? "active" : "") . "><a href='{$url}'>{$i}</a></li>";
                            }

                            if ($page < $page_count) echo "<li><a href='{$nxtUrl}'><i class='fa fa-angle-double-right'></i></a></li>";
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php require_once(BASE_PATH . '/layout/footer.php') ?>