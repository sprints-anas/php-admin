<?php
require_once(BASE_PATH . '/dal/basic_dal.php');
require_once(BASE_PATH . '/logic/posts.php');

function getUsers($sql) {
    if($_SESSION['user']['type'] != 1) {
        header('Location:./index.php');
        die();
    }
    else
    return getRows($sql);
}
function deleteUser($id) {
    $sql = "DELETE FROM users WHERE id=?";
    execute($sql, 'i', [$id]);
}
function blockUser($id) {
    $sql = "UPDATE users SET active = 0 WHERE users.id = ?;";
    execute($sql, 'i', [$id]);
}

function unBlockUser($id) {
    $sql = "UPDATE users SET active = 1 WHERE users.id = ?;";
    execute($sql, 'i', [$id]);
}
?>